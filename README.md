# LibreHealth-Radiology-FHIR
LibreHealth Radiology Fhir is a spring boot application being built with [spring-data](http://projects.spring.io/spring-data/) and [hapi-fhir](http://hapifhir.io/doc_intro.html). 
This application handles the DICOM workflow for [LibreHealth Radiology](https://librehealth.io/projects/lh-radiology/) and exposes REST endpoints for resources of the [Diagnostic Medicine FHIR Module](http://hl7.org/fhir/diagnostics-module.html).   
## First Run the server

### Prerequisites
     maven
     java
     git
    
### Get the code from github

    git clone https://gitlab.com/lehone-hp/lh-radiology-fhir.git

### Build the application
Change working directory

    cd lh-radiology-fhir
    
build the application

    mvn clean package
    
if you want to skip the tests use
    
    mvn clean package -DskipTests=true
    
### Run the application on port 8087
Ensure that no other application is running on 8087 you can change this in src/main/resources/application.properties

    cd target
    
    java -jar lh-radiology-fhir-0.0.1-SNAPSHOT.jar
    
The application deploys a [RestfulServer](http://hapifhir.io/doc_rest_server.html) which listens to GET/PUT/DELETE/POST requests on http://{domain-name}/fhir/*
    
   