package com.librehealth.radiology.fhir.providers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.librehealth.radiology.fhir.dicom.SampleResources;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PatientResourceProvider implements IResourceProvider {

    // Mock patients
    private Map<String, Patient> patientMap;

    public PatientResourceProvider() {
        patientMap = new HashMap<>();
        for (Patient p : parsePatientsFromString(SampleResources.getPatients())) {
            patientMap.put(p.getIdBase(), p);
        }
    }

    public FhirContext getContext() {
        return FhirContext.forDstu3();
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Patient.class;
    }

    /**
     * Search Patient by id
     * @param theId, id of Patient
     * @return Patient with the id
     */
    @Read()
    public Patient getResourceById(@IdParam IdType theId) {

        for (Map.Entry<String, Patient> entry : patientMap.entrySet()) {
            if (entry.getValue().getId().equals(theId.getValue())) {
                return entry.getValue();
            }
        }

        return null;
    }

    @Search
    public List<Patient> getAllPatients() {
        return new ArrayList<>(patientMap.values());
    }

    /**
     * Search patient by comparing theName to Patient's family name and given names
     * @param theName, name of patient
     * @return a list of Patients for whom any of their given names or family names matches theName
     */
    @Search
    public List<Patient> searchByName(@RequiredParam(name=Patient.SP_NAME) StringParam theName) {
        String name = theName.getValue();
        List<Patient> retVal = new ArrayList<>();

        Patient p;
        for (Map.Entry<String, Patient> entry : patientMap.entrySet()) {
            p = entry.getValue();

            for (HumanName humanName: p.getName()) {
                // If the first family name matches, add patient
                if (humanName.getFamily().toLowerCase().contains(name.toLowerCase())) {
                    retVal.add(p);
                    break;
                } else {
                    // Search through given names if any matches
                    for (StringType given : humanName.getGiven()) {
                        if (given.getValueNotNull().toLowerCase().contains(name.toLowerCase())) {
                            retVal.add(p);
                            break;
                        }
                    }
                }
            }
        }

        return retVal;
    }

    /**
     * Parse a List of Strings in which each element is a json string representation of a Patient
     * @param patientStringList, list of Patient json  String
     * @return List of parsed Patients
     */
    private List<Patient> parsePatientsFromString(List<String> patientStringList) {
        IParser jsonParser = getContext().newJsonParser();
        List<Patient> patients = new ArrayList<>();
        for (String p : patientStringList) {
            patients.add(jsonParser.parseResource(Patient.class, p));
        }

        return patients;
    }

}
