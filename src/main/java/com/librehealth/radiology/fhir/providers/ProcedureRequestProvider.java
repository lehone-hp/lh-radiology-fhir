package com.librehealth.radiology.fhir.providers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.ValidationModeEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.librehealth.radiology.fhir.dicom.DicomUtil;
import com.librehealth.radiology.fhir.services.HttpClientService;
import com.pixelmed.dicom.DicomException;
import com.pixelmed.dicom.UIDGenerator;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.exceptions.FHIRException;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.io.IOException;
import java.util.*;

/**
 * Provider for
 */
public class ProcedureRequestProvider implements IResourceProvider {

    // Mock Procedure Requests
    private Map<String, ProcedureRequest> procedureRequestMap;
    private HttpClientService clientService;

    public ProcedureRequestProvider() {
        procedureRequestMap = new HashMap<>();
        clientService = new HttpClientService(getContext());
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return ProcedureRequest.class;
    }

    public FhirContext getContext() {
        return FhirContext.forDstu3();
    }

    @Create
    public MethodOutcome createProcedureRequest(@ResourceParam ProcedureRequest theProcedureRequest) throws DicomException{

        MethodOutcome retVal = new MethodOutcome();
        OperationOutcome outcome = new OperationOutcome();

        String newStudyUid = new UIDGenerator().getNewStudyInstanceUID(null);

        // Generate an Id for the ProcedureRequest
        if (theProcedureRequest.getId() == null) {
            theProcedureRequest.setId(new IdType("ProcedureRequest", UUID.randomUUID().toString()));
            procedureRequestMap.put(theProcedureRequest.getId(), theProcedureRequest);

            retVal.setId(new IdType(theProcedureRequest.getId()));

            //create a new ImagingStudy for the Procedure Request
            ImagingStudy study = new ImagingStudy();
            List<Reference> basedOn = new ArrayList<>();
            basedOn.add(new Reference(theProcedureRequest.getId()));

            study.setBasedOn(basedOn);
            study.setPatient(theProcedureRequest.getSubject());
            study.setReferrer(theProcedureRequest.getRequester().getAgent());
            study.setDescription(theProcedureRequest.getBodySiteFirstRep().primitiveValue());
            study.setUid(newStudyUid);

            try {
                study.setStarted(theProcedureRequest.getOccurrenceDateTimeType().getValue());
            } catch(FHIRException fe) {}

            try {
                clientService.createImagingStudy(study);
            } catch (IOException ioe) {
                outcome.addIssue().setDiagnostics("Unable to create Imaging Study for this Procedure Request!");
            }

        } else {
            outcome.addIssue().setDiagnostics("Procedure Request with ID "+theProcedureRequest.getId()+"" +
                    " already exists!");
        }

        // Create the Modality Work-list for the Procedure Request
        try {
            DicomUtil.createModalityWorklist(theProcedureRequest, newStudyUid);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (outcome.getIssue().size() >= 1) {
            retVal.setOperationOutcome(outcome);
        }
        return retVal;
    }

    /**
     * Returns a ProcedureRequest matching the identifier (theId)
     *
     * @param theId, id of the ProcedureRequest
     * @return an instance of the ProcedureRequest with theId or null if none exists
     */
    @Read()
    public ProcedureRequest getProcedureRequestById(@IdParam IdType theId) {
        for (Map.Entry<String, ProcedureRequest> entry : procedureRequestMap.entrySet()) {
            if (entry.getValue().getId().equals(theId.getValue())) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Search
    public List<ProcedureRequest> getAllProcedureRequests() {
        return new ArrayList<>(procedureRequestMap.values());
    }

    /**
     * Search ProcedureRequests by Patient
     * @param thePatientId is the Id of the Patient
     * @return a list of ProcedureRequests belonging to the Patient or an empty list if none exist
     */
    @Search
    public List<ProcedureRequest> searchByPatientId(@RequiredParam(name=ProcedureRequest.SP_PATIENT) IdType thePatientId) {
        String patientId = thePatientId.getValue();
        List<ProcedureRequest> retVal = new ArrayList<>();

        ProcedureRequest procedureRequest;
        for (Map.Entry<String, ProcedureRequest> entry : procedureRequestMap.entrySet()) {
            procedureRequest = entry.getValue();

            if (procedureRequest.getSubject().getReference().equals("Patient/"+patientId)) {
                retVal.add(procedureRequest);
            }
        }
        return retVal;
    }

    /**
     * Search ProcedureRequests by status
     * @param theStatus is the status of the ProcedureRequest
     * @return a list of ProcedureRequests with corresponding status or an empty list if none exist
     */
    @Search
    public List<ProcedureRequest> searchByStatus(@RequiredParam(name=ProcedureRequest.SP_STATUS) StringType theStatus) {
        String status = theStatus.getValue();
        List<ProcedureRequest> retVal = new ArrayList<>();

        ProcedureRequest procedureRequest;
        for (Map.Entry<String, ProcedureRequest> entry : procedureRequestMap.entrySet()) {
            procedureRequest = entry.getValue();

            if (procedureRequest.getStatus().toCode().equalsIgnoreCase(status)) {
                retVal.add(procedureRequest);
            }
        }
        return retVal;
    }

    /**
     * Replaces an existing ProcedureRequest (by ID) with a new instance (theProcedureRequest)
     * @param theId of existing ProcedureRequest
     * @param theProcedureRequest to replace the existing ProcedureRequest
     * @return
     */
    @Update
    public MethodOutcome update(@IdParam IdType theId, @ResourceParam ProcedureRequest theProcedureRequest) {

        procedureRequestMap.put(theId.getValue(), theProcedureRequest);

        return new MethodOutcome(); //TODO populate this
    }

    @Delete()
    public void deleteProcedureRequest(@IdParam IdType theId) {
        //TODO Delete the procedureRequest
        return;
    }

    @Validate
    public MethodOutcome validateProcedureRquest(@ResourceParam ProcedureRequest theRequest,
                                                 @Validate.Mode ValidationModeEnum theMode,
                                                 @Validate.Profile String theProfile) {
        return new MethodOutcome();
    }
}
