package com.librehealth.radiology.fhir.providers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.annotation.IdParam;
import ca.uhn.fhir.rest.annotation.Read;
import ca.uhn.fhir.rest.annotation.RequiredParam;
import ca.uhn.fhir.rest.annotation.Search;
import ca.uhn.fhir.rest.param.StringParam;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.librehealth.radiology.fhir.dicom.SampleResources;
import org.hl7.fhir.dstu3.model.HumanName;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.Practitioner;
import org.hl7.fhir.dstu3.model.StringType;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("ALL")
public class PractitionerProvider implements IResourceProvider {

    // Mock Practitioners
    private Map<String, Practitioner> practitionerMap;

    public PractitionerProvider() {
        practitionerMap = new HashMap<>();
        for (Practitioner p : parsePractitionersFromString(SampleResources.getPractitioners())) {
            practitionerMap.put(p.getIdBase(), p);
        }
    }

    public FhirContext getContext() {
        return FhirContext.forDstu3();
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Practitioner.class;
    }

    /**
     * Search Practitioner by Id
     * @param theId
     * @return a Practitioner with the Id or null if no practitioner is found
     */
    @Read()
    public Practitioner getResourceById(@IdParam IdType theId) {

        for (Map.Entry<String, Practitioner> entry : practitionerMap.entrySet()) {
            if (entry.getValue().getId().equals(theId.getValue())) {
                return entry.getValue();
            }
        }

        return null;
    }

    @Search
    public List<Practitioner> getAllPractitioners() {
        return new ArrayList<>(practitionerMap.values());
    }

    /**
     * Search practitioner by name
     * @return
     */
    @Search
    public List<Practitioner> searchByName(@RequiredParam(name=Practitioner.SP_NAME) StringParam theName) {
        String name = theName.getValue();
        List<Practitioner> retVal = new ArrayList<>();

        Practitioner p;
        for (Map.Entry<String, Practitioner> entry : practitionerMap.entrySet()) {
            p = entry.getValue();

            for (HumanName humanName: p.getName()) {
                // If the first family name matches, add practitioner
                if (humanName.getFamily().toLowerCase().contains(name.toLowerCase())) {
                    retVal.add(p);
                    break;
                } else {
                    // Search through given names if any matches
                    for (StringType given : humanName.getGiven()) {
                        if (given.getValueNotNull().toLowerCase().contains(name.toLowerCase())) {
                            retVal.add(p);
                            break;
                        }
                    }
                }
            }
        }
        return retVal;
    }

    /**
     * Parse a List of Strings in which each element is a json string representation of a Practitioner
     * @param practitionerStringList, list of Practitioners json  String
     * @return List of parsed Practitioners
     */
    private List<Practitioner> parsePractitionersFromString(List<String> practitionerStringList) {
        IParser jsonParser = getContext().newJsonParser();
        List<Practitioner> retVal = new ArrayList<>();
        for (String p : practitionerStringList) {
            retVal.add(jsonParser.parseResource(Practitioner.class, p));
        }

        return retVal;
    }

}
