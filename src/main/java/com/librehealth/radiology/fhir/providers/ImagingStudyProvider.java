package com.librehealth.radiology.fhir.providers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.ValidationModeEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.librehealth.radiology.fhir.dicom.SampleResources;
import org.hl7.fhir.dstu3.model.IdType;
import org.hl7.fhir.dstu3.model.ImagingStudy;
import org.hl7.fhir.dstu3.model.OperationOutcome;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.*;

public class ImagingStudyProvider implements IResourceProvider {

    // Mock imagingStudies
    private Map<String, ImagingStudy> imagingStudyMap;

    public ImagingStudyProvider() {
        imagingStudyMap = new HashMap<>();
        for (ImagingStudy p : parseImagingStudyFromString(SampleResources.getImagingStudies())) {
            imagingStudyMap.put(p.getIdBase(), p);
        }
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return ImagingStudy.class;
    }

    public FhirContext getContext() {
        return FhirContext.forDstu3();
    }

    @Create
    public MethodOutcome createImagingStudy(@ResourceParam ImagingStudy theImagingStudy) {

        MethodOutcome retVal = new MethodOutcome();

        // Generate an Id for the ProcedureRequest
        if (theImagingStudy.getId() == null) {
            theImagingStudy.setId(new IdType("ImagingStudy", UUID.randomUUID().toString()));
            imagingStudyMap.put(theImagingStudy.getId(), theImagingStudy);
            retVal.setId(new IdType("ImagingStudy", theImagingStudy.getId()));

        } else {
            OperationOutcome outcome = new OperationOutcome();
            outcome.addIssue().setDiagnostics("Imaging Study with ID "+theImagingStudy.getId()+"" +
                    " already exists!");
            retVal.setOperationOutcome(outcome);
        }

        return retVal;
    }

    /**
     * Returns a ImagingStudy matching the identifier (theId)
     *
     * @param theId, id of the ImagingStudy
     * @return an instance of the ImagingStudy with theId or null if none exists
     */
    @Read()
    public ImagingStudy getImagingStudyById(@IdParam IdType theId) {

        for (Map.Entry<String, ImagingStudy> entry : imagingStudyMap.entrySet()) {
            if (entry.getValue().getId().equals(theId.getValue())) {
                return entry.getValue();
            }
        }

        return null;
    }

    @Search
    public List<ImagingStudy> getAllImagingStudies() {
        return new ArrayList<>(imagingStudyMap.values());
    }

    /**
     * Search ImagingStudies by Patient
     * @param thePatientId is the Id of the Patient
     * @return a list of ImagingStudies belonging to the Patient or an empty list if none exist
     */
    @Search
    public List<ImagingStudy> searchByPatientId(@RequiredParam(name=ImagingStudy.SP_PATIENT) IdType thePatientId) {
        String patientId = thePatientId.getValue();
        List<ImagingStudy> retVal = new ArrayList<>();

        ImagingStudy study;
        for (Map.Entry<String, ImagingStudy> entry : imagingStudyMap.entrySet()) {
            study = entry.getValue();

            if (study.getPatient().getReference().equals("Patient/"+patientId)) {
                retVal.add(study);
            }
        }
        return retVal;
    }

    /**
     * Replaces an existing ImagingStudy (by ID) with a new instance (theImagingStudy)
     * @param theId of existing ImagingStudy
     * @param theImagingStudy to replace the existing ImagingStudy
     * @return
     */
    @Update
    public MethodOutcome update(@IdParam IdType theId, @ResourceParam ImagingStudy theImagingStudy) {
        //TODO validate theImagingStudy resource
        //TODO check if resource with thId exists
        //TODO Update the version and last updated time on the resource
        //TODO replace existing resource with theImagingStudy
        return new MethodOutcome(); //TODO populate this
    }

    @Delete()
    public void deleteImagingStudy(@IdParam IdType theId) {
        //TODO Delete the imagingStudy
        return;
    }

    @Validate
    public MethodOutcome validateImagingStudy(@ResourceParam ImagingStudy theImagingStudy,
                                                  @Validate.Mode ValidationModeEnum theMode,
                                                  @Validate.Profile String theProfile) {
        return new MethodOutcome();
    }

    /**
     * Parse a List of Strings in which each element is a json string representation of a ImagingStudy
     * @param imagingStudyStringList, list of ImagingStudy json  String
     * @return List of parsed ImagingStudies
     */
    private List<ImagingStudy> parseImagingStudyFromString(List<String> imagingStudyStringList) {
        IParser jsonParser = getContext().newJsonParser();
        List<ImagingStudy> imagingStudies = new ArrayList<>();
        for (String p : imagingStudyStringList) {
            imagingStudies.add(jsonParser.parseResource(ImagingStudy.class, p));
        }

        return imagingStudies;
    }
}
