package com.librehealth.radiology.fhir.providers;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.api.ValidationModeEnum;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.librehealth.radiology.fhir.services.HttpClientService;
import org.hl7.fhir.dstu3.model.*;
import org.hl7.fhir.instance.model.api.IBaseResource;

import java.util.*;

public class DiagnosticReportProvider implements IResourceProvider {

    // Mock Diagnostic Reports
    private Map<String, DiagnosticReport> reportMap;
    private HttpClientService clientService;

    public DiagnosticReportProvider() {
        reportMap = new HashMap<>();
        clientService = new HttpClientService(getContext());
    }

    public FhirContext getContext() {
        return FhirContext.forDstu3();
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return DiagnosticReport.class;
    }

    @Create
    public MethodOutcome createDiagnosticReport(@ResourceParam DiagnosticReport theDiagnosticReport) {

        MethodOutcome retVal = new MethodOutcome();

        // Generate an Id for the DiagnosticReport
        if (theDiagnosticReport.getId() == null) {
            theDiagnosticReport.setId(new IdType("DiagnosticReport", UUID.randomUUID().toString()));
            reportMap.put(theDiagnosticReport.getId(), theDiagnosticReport);
            retVal.setId(new IdType("DiagnosticReport", theDiagnosticReport.getId()));

        } else {
            OperationOutcome outcome = new OperationOutcome();
            outcome.addIssue().setDiagnostics("DiagnosticReport with ID "+theDiagnosticReport.getId()+"" +
                    " already exists!");
            retVal.setOperationOutcome(outcome);
        }

        return retVal;
    }

    /**
     * Returns a DiagnosticReport matching the identifier (theId)
     *
     * @param theId, id of the DiagnosticReport
     * @return an instance of the DiagnosticReport with theId or null if none exists
     */
    @Read()
    public DiagnosticReport getDiagnosticReportById(@IdParam IdType theId) {
        for (Map.Entry<String, DiagnosticReport> entry : reportMap.entrySet()) {
            if (entry.getValue().getId().equals(theId.getValue())) {
                return entry.getValue();
            }
        }
        return null;
    }

    @Search()
    public DiagnosticReport getReportByImagingStudy(@RequiredParam(name=DiagnosticReport.SP_IMAGE) StringType theImage) {

        String studyId = theImage.getValue();

        DiagnosticReport report;
        for (Map.Entry<String, DiagnosticReport> entry : reportMap.entrySet()) {

            report = entry.getValue();
            if (report.getImagingStudyFirstRep().getReference().equals("ImagingStudy/"+studyId)) {
                return report;
            }
        }
        return null;
    }

    @Search()
    public DiagnosticReport getReportByProcedureRequest(@RequiredParam(name=DiagnosticReport.SP_BASED_ON) StringType thePRId) {

        String studyId = thePRId.getValue();

        DiagnosticReport report;
        for (Map.Entry<String, DiagnosticReport> entry : reportMap.entrySet()) {

            report = entry.getValue();
            for (Reference basedOn : report.getBasedOn()) {
                if (basedOn.getReference().equals("ProcedureRequest/" + studyId)) {
                    return report;
                }
            }
        }
        return null;
    }

    /**
     * Returns all the reports
     */
    @Search
    public List<DiagnosticReport> search() {
        return new ArrayList<>(reportMap.values());
    }

    /**
     * Returns all the reports
     */
    @Search
    public List<DiagnosticReport> searchByPatientId(@RequiredParam(name=DiagnosticReport.SP_PATIENT) IdType thePatientId) {
        String patientId = thePatientId.getValue();
        List<DiagnosticReport> retVal = new ArrayList<>();

        DiagnosticReport report;
        for (Map.Entry<String, DiagnosticReport> entry : reportMap.entrySet()) {
            report = entry.getValue();

            if (report.getSubject().getReference().equals("Patient/"+patientId)) {
                retVal.add(report);
            }
        }
        return retVal;
    }


    /**
     * Replaces an existing DiagnosticReport (by ID) with a new instance (theDiagnosticReport)
     * @param theId of existing DiagnosticReport
     * @param theDiagnosticReport to replace the existing DiagnosticReport
     * @return
     */
    @Update
    public MethodOutcome update(@IdParam IdType theId, @ResourceParam DiagnosticReport theDiagnosticReport) {

        reportMap.put(theId.getValue(), theDiagnosticReport);

        return new MethodOutcome(); //TODO populate this
    }

    @Delete()
    public void deleteDiagnosticReport(@IdParam IdType theId) {
        //TODO Delete the diagnosticReport
        return;
    }


    @Validate
    public MethodOutcome validateDiagnosticReport(@ResourceParam DiagnosticReport theReport,
                                                  @Validate.Mode ValidationModeEnum theMode,
                                                  @Validate.Profile String theProfile) {
        return new MethodOutcome();
    }
}
