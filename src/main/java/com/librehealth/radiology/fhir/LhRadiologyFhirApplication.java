package com.librehealth.radiology.fhir;

import com.librehealth.radiology.fhir.sevlet.RadiologyRestfulServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class LhRadiologyFhirApplication {

	public static void main(String[] args) {
		SpringApplication.run(LhRadiologyFhirApplication.class, args);
	}

	/**
	 * Create a servlet for the RadiologyRestfulServer which handles incomming
	 * requests with url pattern (/fhir/*)
	 */
	@Bean
	public ServletRegistrationBean exampleServletBean() {
		ServletRegistrationBean bean = new ServletRegistrationBean(
				new RadiologyRestfulServer(), "/fhir/*");
		bean.setLoadOnStartup(2);
		return bean;
	}
}
