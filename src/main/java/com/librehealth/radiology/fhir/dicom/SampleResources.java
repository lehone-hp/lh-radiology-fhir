package com.librehealth.radiology.fhir.dicom;

import java.util.ArrayList;
import java.util.List;

public abstract class SampleResources {

    public static List<String> getPatients() {
        List<String> retVal = new ArrayList<>();
        retVal.add(patient1);
        retVal.add(patient2);
        return retVal;
    }

    public static List<String> getPractitioners() {
        List<String> retVal = new ArrayList<>();
        retVal.add(practitioner1);
        retVal.add(practitioner2);
        return retVal;
    }

    public static List<String> getImagingStudies() {
        List<String> retVal = new ArrayList<>();
        retVal.add(imagingStudy1);
        //retVal.add(imagingStudy2);
        //retVal.add(imagingStudy3);
        //retVal.add(imagingStudy4);
        //retVal.add(imagingStudy5);
        return retVal;
    }

    /**
     * Sample json patients
     */
    private static final String patient1 = "{\n" +
            "  \"resourceType\": \"Patient\",\n" +
            "  \"id\": \"3711014\",\n" +
            "  \"text\": {\n" +
            "    \"status\": \"generated\",\n" +
            "    \"div\": \"<div xmlns=\\\"http://www.w3.org/1999/xhtml\\\"> </div>\"\n" +
            "  },\n" +
            "  \"identifier\": [\n" +
            "    {\n" +
            "      \"use\": \"usual\",\n" +
            "      \"type\": {\n" +
            "        \"text\": \"Computer-Stored Abulatory Records (COSTAR)\"\n" +
            "      },\n" +
            "      \"value\": \"10006579\",\n" +
            "      \"assigner\": {\n" +
            "        \"display\": \"AccMgr\"\n" +
            "      }\n" +
            "    }\n" +
            "  ],\n" +
            "  \"name\": [\n" +
            "    {\n" +
            "      \"use\": \"official\",\n" +
            "      \"text\": \"Peter Martin Nicolas\"," +
            "      \"family\": \"Peter Martin\",\n" +
            "      \"given\": [\n" +
            "        \"Nicolas\"\n" +
            "      ]\n" +
            "    }\n" +
            "  ]," +
            "  \"active\": true,\n" +
            "  \"gender\": \"male\",\n" +
            "  \"birthDate\": \"1995-10-10\",\n" +
            "  \"deceasedBoolean\": false\n" +
            "}";

    private static final String patient2 = "{\n" +
            "  \"resourceType\": \"Patient\",\n" +
            "  \"id\": \"3711013\",\n" +
            "  \"text\": {\n" +
            "    \"status\": \"generated\",\n" +
            "    \"div\": \"<div xmlns=\\\"http://www.w3.org/1999/xhtml\\\"> </div>\"\n" +
            "  },\n" +
            "  \"identifier\": [\n" +
            "    {\n" +
            "      \"use\": \"usual\",\n" +
            "      \"type\": {\n" +
            "        \"text\": \"Computer-Stored Abulatory Records (COSTAR)\"\n" +
            "      },\n" +
            "      \"value\": \"10006579\",\n" +
            "      \"assigner\": {\n" +
            "        \"display\": \"AccMgr\"\n" +
            "      }\n" +
            "    }\n" +
            "  ],\n" +
            "  \"name\": [\n" +
            "    {\n" +
            "      \"use\": \"official\",\n" +
            "      \"text\": \"Nathan Cooper\",\n" +
            "      \"family\": \"Cooper\",\n" +
            "      \"given\": [\n" +
            "          \"Nathan\"\n" +
            "        ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"active\": true,\n" +
            "  \"gender\": \"male\",\n" +
            "  \"birthDate\": \"1924-10-10\",\n" +
            "  \"deceasedBoolean\": false\n" +
            "}";

    /**
     * Practitioners
     */
    private static final String practitioner1 = "{\n" +
            "  \"resourceType\": \"Practitioner\",\n" +
            "  \"id\": \"cc-prac-carlson-john\",\n" +
            "  \"active\": true,\n" +
            "  \"name\": [\n" +
            "    {\n" +
            "      \"text\": \"Dr. John Carlson, MD\",\n" +
            "      \"family\": \"Carlson\",\n" +
            "      \"given\": [\n" +
            "        \"John\"\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"telecom\": [\n" +
            "    {\n" +
            "      \"system\": \"phone\",\n" +
            "      \"value\": \"777-555-1212\",\n" +
            "      \"use\": \"work\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"system\": \"email\",\n" +
            "      \"value\": \"john.carlson@woohoo.com\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"gender\": \"male\"\n" +
            "}";

    private static final String practitioner2 = "{\n" +
            "  \"resourceType\": \"Practitioner\",\n" +
            "  \"id\": \"cc-prac-john-doe\",\n" +
            "  \"active\": true,\n" +
            "  \"name\": [\n" +
            "    {\n" +
            "      \"text\": \"Dr. John Doe\",\n" +
            "      \"family\": \"Doe\",\n" +
            "      \"given\": [\n" +
            "        \"John\"\n" +
            "      ]\n" +
            "    }\n" +
            "  ],\n" +
            "  \"telecom\": [\n" +
            "    {\n" +
            "      \"system\": \"phone\",\n" +
            "      \"value\": \"675-555-1212\",\n" +
            "      \"use\": \"work\"\n" +
            "    },\n" +
            "    {\n" +
            "      \"system\": \"email\",\n" +
            "      \"value\": \"john.doe@hotmail.com\"\n" +
            "    }\n" +
            "  ],\n" +
            "  \"gender\": \"male\"\n" +
            "}";

    private static final String imagingStudy1 = "{\n" +
            "        \"resourceType\": \"ImagingStudy\",\n" +
            "        \"id\": \"20180604\",\n" +
            "        \"uid\": \"1.3.6.1.4.1.5962.99.1.354.9766.1528258657830.42.2.604\",\n" +
            "        \"patient\": {\n" +
            "          \"reference\": \"Patient/3711014\"\n" +
            "        },\n" +
            "        \"referrer\": {\n" +
            "          \"reference\": \"Practitioner/cc-prac-carlson-john\"\n" +
            "        },\n" +
            "        \"numberOfSeries\": 1,\n" +
            "        \"description\": \"Upper GI Series\"\n" +
            "      }";

    private static final String imagingStudy2 = "{\n" +
            "        \"resourceType\": \"ImagingStudy\",\n" +
            "        \"id\": \"20180605\",\n" +
            "        \"uid\": \"1.3.6.1.4.1.5962.99.1.354.9766.1528258657830.50.2.605\",\n" +
            "        \"patient\": {\n" +
            "          \"reference\": \"Patient/3711013\"\n" +
            "        },\n" +
            "        \"referrer\": {\n" +
            "          \"reference\": \"Practitioner/cc-prac-john-doe\"\n" +
            "        },\n" +
            "        \"numberOfSeries\": 1,\n" +
            "        \"description\": \"8 images of 4 biplane acqs\"\n" +
            "      }";

    private static final String imagingStudy3 = "{\n" +
            "        \"resourceType\": \"ImagingStudy\",\n" +
            "        \"id\": \"20180606\",\n" +
            "        \"uid\": \"1.3.6.1.4.1.5962.99.1.354.9766.1528258657830.51.2.606\",\n" +
            "        \"patient\": {\n" +
            "          \"reference\": \"Patient/3711013\"\n" +
            "        },\n" +
            "        \"referrer\": {\n" +
            "          \"reference\": \"Practitioner/cc-prac-carlson-john\"\n" +
            "        },\n" +
            "        \"numberOfSeries\": 365,\n" +
            "        \"numberOfInstances\": 1,\n" +
            "        \"description\": \"RT ANKLE\"\n" +
            "      }";

    private static final String imagingStudy4 = "{\n" +
            "        \"resourceType\": \"ImagingStudy\",\n" +
            "        \"id\": \"20180607\",\n" +
            "        \"uid\": \"1.3.6.1.4.1.5962.99.1.354.9766.1528258657830.52.2.607\",\n" +
            "        \"patient\": {\n" +
            "          \"reference\": \"Patient/3711014\"\n" +
            "        },\n" +
            "        \"referrer\": {\n" +
            "          \"reference\": \"Practitioner/cc-prac-carlson-john\"\n" +
            "        },\n" +
            "        \"numberOfSeries\": 1,\n" +
            "        \"description\": \"CT Head\"\n" +
            "      }\n";

    private static final String imagingStudy5 = "{\n" +
            "        \"resourceType\": \"ImagingStudy\",\n" +
            "        \"id\": \"20180608\",\n" +
            "        \"uid\": \"1.3.6.1.4.1.5962.99.1.354.9766.1528258657830.53.2.608\",\n" +
            "        \"patient\": {\n" +
            "          \"reference\": \"Patient/3711013\"\n" +
            "        },\n" +
            "        \"referrer\": {\n" +
            "          \"reference\": \"Practitioner/cc-prac-john-doe\"\n" +
            "        },\n" +
            "        \"numberOfSeries\": 1,\n" +
            "        \"description\": \"BRAIN\"\n" +
            "      }\n";
}
