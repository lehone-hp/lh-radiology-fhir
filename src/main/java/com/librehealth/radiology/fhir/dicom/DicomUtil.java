package com.librehealth.radiology.fhir.dicom;

import ca.uhn.fhir.context.FhirContext;
import com.librehealth.radiology.fhir.services.HttpClientService;
import com.pixelmed.dicom.*;
import org.hl7.fhir.dstu3.model.CodeableConcept;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Practitioner;
import org.hl7.fhir.dstu3.model.ProcedureRequest;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DicomUtil {

    private static FhirContext fhirContext = FhirContext.forDstu3();

    private static final String sampleWorklistPath = "src/main/resources/sample-worklist.wl";

    private static final String worklistDatabaseRoot = "Worklists";

    /**
     * Create a DICOM Modality Worklist from ProcedureRequest
     * Sets the name of the file to be the UUID of the ProcdureRequest
     * @param procedureRequest The ProcedureRequest to be used to create the Modality Worklist
     */
    public static void createModalityWorklist(ProcedureRequest procedureRequest, String studyUid) {
        String fileName = procedureRequest.getIdBase().split("/")[1] + ".wl";
        createModalityWorklist(fileName, procedureRequest, studyUid);
    }

    public static void createModalityWorklist(String fileName, ProcedureRequest procedureRequest, String studyUid) {
        HttpClientService clientService = new HttpClientService(fhirContext);

        try {
            Patient patientSubject;
            Practitioner requester;
            Practitioner performer;

            final String patientId = procedureRequest.getSubject().getReference();
            final String performerId = procedureRequest.getPerformer().getReference();
            final String requesterId = procedureRequest.getRequester().getAgent().getReference();

            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");

            // Get Patient, Requester and Performer resources via REST
            try{
                patientSubject = clientService.getPatientById(patientId);
                performer = clientService.getPractitionerById(performerId);
                requester = clientService.getPractitionerById(requesterId);
            } catch(IOException ioe) {
                throw new IOException(ioe);
            }

            // Get Attributes from sample DICOM ModalityWorklist file
            AttributeList defaultAttributes = getAttributes(sampleWorklistPath);

            //Replace the default attribute values with values from the ProcedureRequest
            /**
             * Full list of Dicom Modality Worklist attributes
             * @see <a href="http://dicom.nema.org/medical/dicom/2014c/output/chtml/part04/sect_K.6.html#table_K.6-1">
             *     K Basic Worklist Management Service</a>
             *
             * Attributes for Patient Identification
             */
            defaultAttributes.replace(TagFromName.PatientName, patientSubject.getNameFirstRep().getNameAsSingleString());
            defaultAttributes.replace(TagFromName.PatientID, patientId);

            /**
             * Attributes for Patient Demographics
             */
            defaultAttributes.replace(TagFromName.PatientBirthDate, sdf.format(patientSubject.getBirthDate()));
            if (patientSubject.getGender().getDisplay().equalsIgnoreCase("male")){
                defaultAttributes.replace(TagFromName.PatientSex, "M");
            } else {
                defaultAttributes.replace(TagFromName.PatientSex, "F");
            }

            /**
             * Attributes for request procedure
             */
            defaultAttributes.replace(TagFromName.RequestedProcedureID, procedureRequest.getIdBase());
            try{
                defaultAttributes.replace(TagFromName.RequestedProcedureComments,
                        procedureRequest.getNoteFirstRep().getText());
            } catch(NullPointerException npe) {}

            defaultAttributes.replace(TagFromName.StudyInstanceUID, studyUid);
            defaultAttributes.replace(TagFromName.RequestedProcedurePriority, procedureRequest.getPriority().getDisplay());
            defaultAttributes.replace(TagFromName.RequestedProcedureDescription, procedureRequest.getIntent().toCode());

            /**
             * Attributes for Imaging Service Request
             */
            defaultAttributes.replace(TagFromName.AccessionNumber, "1");    //TODO change this
            defaultAttributes.replace(TagFromName.RequestingPhysician, requester.getNameFirstRep().getNameAsSingleString());
            defaultAttributes.replace(TagFromName.ReferringPhysicianName, requester.getNameFirstRep().getNameAsSingleString());
            CodeableConcept code = procedureRequest.getCode();
            defaultAttributes.replace(TagFromName.Modality, code.getCodingFirstRep().getCode());
            defaultAttributes.replace(TagFromName.PerformingPhysicianName,
                    performer.getNameFirstRep().getNameAsSingleString());

            Date occurrence = procedureRequest.getOccurrenceDateTimeType().getValue();
            String date = sdf.format(occurrence);
            sdf = new SimpleDateFormat("hhmmss");
            String time = sdf.format(occurrence);

            defaultAttributes.replace(TagFromName.StudyDate, date);
            defaultAttributes.replace(TagFromName.StudyTime, time);

            // Write the Attributes to a DICOM Modality Worklist file
            defaultAttributes.write(worklistDatabaseRoot+"/"+fileName, TransferSyntax.ExplicitVRLittleEndian, true, true);

            System.out.println("\n\n");
            getAttributes(worklistDatabaseRoot+"/"+fileName);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Extract and return a list of attributes from a DICOM File
     * @param dicomFile, absolute path to the DICOM file
     * @return List of DICOM Attributes
     * @throws DicomException
     * @throws IOException
     */
    public static AttributeList getAttributes(String dicomFile) throws DicomException, IOException{
        AttributeList list = new AttributeList();

        try {
            list.read(dicomFile);
            System.out.println(list.toString());
        } catch (DicomException de) {
            throw new DicomException(de.getLocalizedMessage());
        } catch (IOException ioe) {
            throw new IOException(ioe);
        }

        return list;
    }

}

