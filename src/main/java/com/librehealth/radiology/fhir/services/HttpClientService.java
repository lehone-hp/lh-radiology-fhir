package com.librehealth.radiology.fhir.services;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import org.hl7.fhir.dstu3.model.ImagingStudy;
import org.hl7.fhir.dstu3.model.Patient;
import org.hl7.fhir.dstu3.model.Practitioner;
import org.hl7.fhir.dstu3.model.ProcedureRequest;

import java.io.IOException;

public class HttpClientService {

    private String serverUrl = "http://localhost:8087/fhir";

    // Create a client (only needed once)
    private FhirContext fhirContext;
    private IGenericClient client;

    public HttpClientService(FhirContext context) {
        this.fhirContext = context;
        client = fhirContext.newRestfulGenericClient(serverUrl);
    }

    public ProcedureRequest getProcedureRequestById(String id) throws IOException {
        // Invoke the client
        return client.read()
                .resource(ProcedureRequest.class)
                .withId(id)
                .prettyPrint().execute();
    }

    public Patient getPatientById(String id) throws IOException{
        // Invoke the client
        return client.read()
                .resource(Patient.class)
                .withId(id)
                .prettyPrint().execute();
    }

    public Practitioner getPractitionerById(String id) throws IOException{
        // Invoke the client
        return client.read()
                .resource(Practitioner.class)
                .withId(id)
                .execute();
    }

    public ImagingStudy getImagingStudyById(String id) throws IOException{
        // Invoke the client
        return client.read()
                .resource(ImagingStudy.class)
                .withId(id)
                .execute();
    }

    public MethodOutcome createImagingStudy(ImagingStudy imagingStudy) throws IOException {
        return client.create().resource(imagingStudy).execute();
    }
}
